﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LerpCounterLag : MonoBehaviour
{

    public Transform m_affected;
    public Transform m_target;
    [Range(1.1f,3f)]
    public float m_reducePrediction = 1.8f;

    private DebugInfo m_debugInfo = new DebugInfo();
    [System.Serializable]
    public class DebugInfo
    {
        public Queue<PointAndTime> m_points = new Queue<PointAndTime>();
        public float m_timePreviousChange;
        public float m_timeBetweenFrame = 0f;

        public Vector3 m_previousTargetPosition;
        public Quaternion m_previousTargetRotation;
        public Vector3 m_previousAffectedPosition;
        public Quaternion m_previousAffectedRotation;

        public Vector3 m_lastDirection;
        public Quaternion m_lastRotation;

        public float m_pourcent;
    }

    public struct PointAndTime {
        public Vector3 m_position;
        public Quaternion m_rotation;
        public float m_time;

        public PointAndTime(Vector3 position, Quaternion rotation, float time) 
        {
            this.m_position = position;
            this.m_rotation = rotation;
            this.m_time = time;
        }
    }

    void SaveKey()
    {
        m_debugInfo.m_points.Enqueue(new PointAndTime( m_target.position, m_target.rotation, GetTime()));
    }

    private float GetTime()
    {
        return Time.time;
    }

    private void Start()
    {
        m_debugInfo.m_previousAffectedPosition = Vector3.zero;
        m_target.position = Vector3.zero;
        m_debugInfo.m_lastDirection = Vector3.zero;
    }

    void Update()
    {
        if (TargetMoved() || TargetRotated())
        {
            SaveKey();
            ComputePreviousOrientation();
            RecordTimeBetweenFrame();
            RecordPreviousPositionAndRotation();
        }

        ComputePourcentToLerp();
        LerpAffectedObjectToTargetPreduction();
    }

    private void LerpAffectedObjectToTargetPreduction()
    {
        if (m_debugInfo.m_pourcent > 0f) {
            m_affected.position = Vector3.Lerp(m_debugInfo.m_previousAffectedPosition, m_target.position + (m_debugInfo.m_lastDirection), m_debugInfo.m_pourcent / m_reducePrediction);
            m_affected.rotation = Quaternion.Lerp(m_debugInfo.m_previousAffectedRotation, m_target.rotation * m_debugInfo.m_lastRotation, m_debugInfo.m_pourcent / m_reducePrediction);
        }
    }

    private void ComputePourcentToLerp()
    {
        m_debugInfo.m_pourcent = (GetTime() - m_debugInfo.m_timePreviousChange) / (m_debugInfo.m_timeBetweenFrame * 2f);
    }

    private void RecordTimeBetweenFrame()
    {
        m_debugInfo.m_timeBetweenFrame = GetTime() - m_debugInfo.m_timePreviousChange;
        m_debugInfo.m_timePreviousChange = GetTime();
    }

    private void ComputePreviousOrientation()
    {
        m_debugInfo.m_lastDirection = m_target.position - m_debugInfo.m_previousTargetPosition;
        m_debugInfo.m_lastRotation = GetDifferenceOfQuaternion(m_target.rotation, m_target.rotation);
    }

    private void RecordPreviousPositionAndRotation()
    {
        m_debugInfo.m_previousTargetPosition = m_target.position;
        m_debugInfo.m_previousTargetRotation = m_target.rotation;
        m_debugInfo.m_previousAffectedPosition = m_affected.position;
        m_debugInfo.m_previousAffectedRotation = m_affected.rotation;
    }

    private bool TargetRotated()
    {
        return Quaternion.Angle(m_target.rotation, m_debugInfo.m_previousTargetRotation) > 0.1f;
    }

    private bool TargetMoved()
    {
        return Vector3.Distance(m_target.position, m_debugInfo.m_previousTargetPosition)>0.01f;
    }
    public Quaternion GetDifferenceOfQuaternion(Quaternion a, Quaternion b)
    {
        return a * Quaternion.Inverse(b);
    }
    public Quaternion ApplyRotationToQuaterion(Quaternion toA, Quaternion byB)
    {
        return toA * byB;
    }
}
