﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowWithLag : MonoBehaviour
{
    public Transform m_target;
    public float m_delay=0.1f;


    IEnumerator Start()
    {
        while (true)
        {
            yield return new WaitForEndOfFrame();
            yield return new WaitForSeconds(m_delay);
            Sync();

        }
    }
    
    void Sync()
    {
        transform.position = m_target.position;
        transform.rotation = m_target.rotation;


    }
}
