﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomRotation : MonoBehaviour
{
    public float m_minAngle = 10f;
    public float m_maxAngle = 45;
    Vector3 m_rotation;

    void Start()
    {
        m_rotation = new Vector3(GetRandomAngle(m_minAngle, m_maxAngle), GetRandomAngle(m_minAngle, m_maxAngle), GetRandomAngle(m_minAngle, m_maxAngle));
    }

    private float GetRandomAngle(float minAngle, float maxAngle)
    {
        return UnityEngine.Random.Range(minAngle, maxAngle) * UnityEngine.Random.Range(-1f, 1f);
    }    void Update()
    {
        transform.Rotate(m_rotation * Time.deltaTime);
        
    }
}
